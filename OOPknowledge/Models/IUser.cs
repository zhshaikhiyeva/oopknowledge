﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPknowledge.Models
{
    public interface IUser
    {
        public string Auth();
        public void Authentic();
        public string login{ get; set; }
        public string password { get; set; }
        public string token { get; set; }
        public void LogOut();
        public Details Details { get; set; }
    }
}
