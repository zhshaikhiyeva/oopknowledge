﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOPknowledge.Models
{
    public class BaseDbContext: DbContext 
    {
        public DbSet<Users> Users { get; set; }
        public DbSet<Details> Details { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"Server=(localDb)\MSSQLLocalDB;Database=Master;Trusted_Connection=True;MultipleActiveResultSets=true;");
        }
    }
}
