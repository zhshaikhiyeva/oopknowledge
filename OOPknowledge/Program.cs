﻿using Microsoft.EntityFrameworkCore;
using OOPknowledge.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace OOPknowledge
{
    class Program
    {
        static BaseDbContext db = new BaseDbContext();
        public static List<Users> Get(int id)
        {
            return db.Users.Skip(id*10).Take(10).ToList();
        }

        public static int Post()
        {
            try
            {
                db.Add(new Users() { login = "asd", password = "asd", token = "asd", DetailsId = 2 });
                db.SaveChanges();
                return 0;
            }
            catch (Exception ex) {
                return -1;
            }
        }

        public static int PostDetails()
        {
            try
            {
                db.Details.Add(new Details() { FIO = "aa bbb cccc", BirthDate = DateTime.Parse("02.12.2012") });
                db.SaveChanges();
                return 0;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public static int Put()
        {
            try
            {
                Users u = new Users();
                u.Id = 3;
                u.login = "test";
                #region sample 1
                //db.Attach(u);
                //db.Entry(u).Property(el => el.login).IsModified = true;

                #endregion
                #region sample 2
                Users u1 = db.Users.SingleOrDefault(m => m.Id == 5);
                //u1.login = "test2";
                u1.DetailsId = 1;
                db.Entry(u1).State = EntityState.Modified;
                #endregion
                db.SaveChanges();

                return 0;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public static int Del()
        {
            try
            {
                Users users = db.Users.SingleOrDefault(m => m.Id == 3);
                db.Users.Remove(users);


                db.SaveChanges();
                return 0;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        
        public static void ReadDataFromDb()
        {
            PostDetails();
        }



        static void Main(string[] args)
        {
            

           // ReadDataFromDb();
           // string[] s = File.ReadAllLines(Path.Combine("", @"C:/Users/Zhazira/source/repos/OOPknowledge/OOPknowledge/Data/user.txt"));
           // string[] s2 = File.ReadAllLines(Path.Combine("", @"C:/Users/Zhazira/source/repos/OOPknowledge/OOPknowledge/Data/Details.txt"));

            //List<Users> user = new List<Users>();
            //List<Details> details = new List<Details>();

           /* foreach (var el in s)
            {
                string[] t = el.Split(' ');

                user.Add(new Users(t[1], t[2], t[3], int.Parse(t[4])));
            }

            foreach (var el in s2)
            {
                string[] t2 = el.Split(' ');

                details.Add(new Details(int.Parse(t2[0]), t2[1], DateTime.Parse(t2[2])));
            }*/

            //var userDetails = user.Join(details,
            //    x => x.DetailsId,
            //    y => y.id,
            //    (x, y) => new Users()
            //    {
            //        login = x.login,
            //        DetailsId = x.DetailsId,
            //        password = x.password,
            //        token = x.token,
                 
            //        Details = y
            //    });

            //var user1 = userDetails.FirstOrDefault();

            //Console.WriteLine(user1.login);
        }
    }
}
