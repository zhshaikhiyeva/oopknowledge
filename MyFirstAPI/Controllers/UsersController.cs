﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyFirstAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyFirstAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private BaseDbContext db;

        public UsersController(BaseDbContext context) {
            db = context;
        }

        // GET: api/<UsersController>
        [HttpGet]
        public IEnumerable<Users> Get()
        {

            return db.Users.ToList();
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public Users Get(string id)
        {
            return db.Users.FirstOrDefault(el => el.login == id); ;
        }

        // POST api/<UsersController>
        [HttpPost]
        public string Post([FromBody] Users value)
        {
            try
            {
                db.Users.Add(value);
                db.SaveChanges();
                return "OK";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }

        }

        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public string Put(int id, [FromBody] Users value)
        {
            try
            {
                value.Id = id;
                db.Attach(value);
                db.Entry(value).Property(el => el.login).IsModified = true;
                db.SaveChanges();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
