﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstAPI.Models
{
    public class Details
    {

        public Details() { }
        public Details(int _id, string _FIO, DateTime _BirthDate)
        {
            id = _id;
            FIO = _FIO;
            BirthDate = _BirthDate;

        }
        public int? id { get; set; }
        public string FIO { get; set; }

        public DateTime BirthDate { get; set; }



    }
}
