﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstAPI.Models
{
    public class BaseDbContext : DbContext
    {
        public BaseDbContext() { }
        public BaseDbContext(DbContextOptions<BaseDbContext> options) : base(options)
        {
        } 
        public DbSet<Users> Users { get; set; }
        public DbSet<Details> Details { get; set; }

    }
}
