﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstAPI.Models
{
    public class Users
    {

        public Users() { }
        public Users(string _login, string _password, string _token, int _DetailsId)
        {
            login = _login;
            password = _password;
            token = _token;
            DetailsId = _DetailsId;


        }

        public int? Id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string token { get; set; }

        Details _details = null;

        public Details Details
        {
            get
            {
                return _details;

            }
            set
            {

                _details = value;
            }
        }

        public int? DetailsId { get; set; }

    }
}
